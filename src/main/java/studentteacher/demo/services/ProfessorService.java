package studentteacher.demo.services;


import org.springframework.stereotype.Service;
import studentteacher.demo.models.Professor;
import studentteacher.demo.models.Student;

import java.util.List;
@Service
public interface ProfessorService {
    Professor addProfessor(Professor professor);

    Professor getProfessor(Long id);

    List<Student> getAllStudents();

    void deleteProfessor(Professor professor);

    void deleteProfessor(Long id);
}
