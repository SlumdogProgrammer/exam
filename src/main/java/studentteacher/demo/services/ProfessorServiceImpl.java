package studentteacher.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import studentteacher.demo.models.Professor;
import studentteacher.demo.models.Student;
import studentteacher.demo.repositories.ProfessorRepository;

import java.util.List;
@Service 
public class ProfessorServiceImpl implements ProfessorService {

    @Autowired
    private ProfessorRepository professorRepository;

    @Override
    public Professor addProfessor(Professor professor) {
        return professorRepository.save(professor);
    }

    @Override
    public Professor getProfessor(Long id) {
        return professorRepository.getOne(id);
    }

    @Override
    public List<Professor> getAllStudents() {
        return professorRepository.findAll();
    }

    @Override
    public void deleteProfessor(Professor professor) {
        professorRepository.delete(professor);
    }

    @Override
    public void deleteProfessor(Long id) {
        professorRepository.deleteById(id);
    }
}
