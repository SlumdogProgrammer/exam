package studentteacher.demo.services;

import org.springframework.stereotype.Service;
import studentteacher.demo.models.Student;

import java.util.List;
@Service
public interface StudentService {
    Student addStudent(Student student);

    Student getStudent(Long id);

    List<Student> getAllStudents();

    void deleteStudent(Student student);

    void deleteStudent(Long id);
}
