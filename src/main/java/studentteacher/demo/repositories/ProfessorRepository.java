package studentteacher.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import studentteacher.demo.models.Professor;

public interface ProfessorRepository
        extends JpaRepository<Professor,Long> {
}
