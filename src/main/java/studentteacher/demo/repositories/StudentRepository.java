package studentteacher.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import studentteacher.demo.models.Student;

public interface StudentRepository
        extends JpaRepository<Student,Long> {

}
