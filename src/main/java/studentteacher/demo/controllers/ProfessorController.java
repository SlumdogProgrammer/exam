package studentteacher.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import studentteacher.demo.models.Professor;
import studentteacher.demo.models.Student;
import studentteacher.demo.services.ProfessorService;

import java.util.List;

@RestController
@RequestMapping("/professors")
public class ProfessorController {
    private final ProfessorService professorService;
    @Autowired
    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @PostMapping("/new")
    public void newProfessor(@ModelAttribute("professor") Professor professor){
        professorService.addProfessor(professor);
    }

    @GetMapping("/{id}")
    public Professor getProfessor(@PathVariable("id") Long id) {
        return professorService.getProfessor(id);
    }

    @GetMapping
    public List<Student> getAllStudents() {
        return professorService.getAllStudents();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id){
        professorService.deleteProfessor(id);
    }

}
