package studentteacher.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import studentteacher.demo.models.Professor;
import studentteacher.demo.models.Student;
import studentteacher.demo.services.StudentService;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
    private final StudentService studentService;
    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/new")
    public void newProfessor(@ModelAttribute("professor") Student student){
        studentService.addStudent(student);
    }

    @GetMapping("/{id}")
    public Student getProfessor(@PathVariable("id") Long id) {
        return studentService.getStudent(id);
    }

    @GetMapping
    public List<Professor> getAllProfessors() {
        return studentService.getAllStudents();
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable("id") Long id){
        studentService.deleteStudent(id);
    }
}
