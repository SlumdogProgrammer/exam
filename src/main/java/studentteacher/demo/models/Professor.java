package studentteacher.demo.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "professor")
public class Professor {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "prof_name")
    private String name;
    @Column(name = "degree")
    private String degree;
    @Column(name = "salary")
    private int salary;
    @OneToMany(mappedBy = "professor")
    Set<StudentProfessor> header;

    public Professor(){}

    public Professor(String name, String degree, int salary) {
        this.name = name;
        this.degree = degree;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
