package studentteacher.demo.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Student {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "student_name")
    private String name;
    @Column(name = "qualification")
    private String qualification;
    @OneToMany(mappedBy = "student")
    Set<StudentProfessor> header;

    public Student(){}

    public Student(String name, String qualification){
        this.name=name;
        this.qualification=qualification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }


}
