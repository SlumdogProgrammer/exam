package studentteacher.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "studentprofessor")
public class StudentProfessor {
    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor professor;

    @Column(name = "is_header")
    private boolean header;
}
