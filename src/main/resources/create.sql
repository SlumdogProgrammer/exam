create table Professor(
    professor_id integer primary key,
    prof_name char(30),
    degree char(30),
    salary integer check ( salary>1 and salary < 100000 )
);
create table Student(
    student_id integer primary key,
    student_name char(30),
    qualification char(30)
);
create table StudentProfessor(
   student_professor_id integer primary key,
   student_id integer,
   professor_id integer,
   is_header bool,
   foreign key (student_id) references Student (student_id),
   foreign key (professor_id) references Professor (professor_id)
);